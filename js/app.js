/*import * as THREE from 'three'; */



//defining the variables so that they can be used later in the code 
let container;
let scene;
let camera;
let mesh;
let renderer;
let controls;
let cube;


function createCamera () {
    // Initialize the values needed for the camera
    const fov = 35; // Field of View, can go from 1 to 179 degrees
    const aspect = container.clientWidth / container.clientHeight;
    const near = 0.1; // the near clipping plane
    const far = 100; // the far clipping plane. it has to be higher than the `near`
    
    // Then create the camera
    camera = new THREE.PerspectiveCamera( fov, aspect, near, far );

    // every object is initially created at ( 0, 0, 0 )
    // we'll move the camera back a bit so that we can view the scene
    // our screen is now the plan defined by z=10.
    camera.position.set(0,0,25);
}

function createLights () {
    const ambientLight = new THREE.HemisphereLight(
        0xddeeff, // sky color
        0x202020, // ground color
        5, // intensity
    );
    
    const mainLight = new THREE.DirectionalLight( 0xffffff, 5 );
    mainLight.position.set( 10, 10, 10 );
    
    scene.add( ambientLight, mainLight )

/*
    const ambientLight = new THREE.HemisphereLight(
        0xddeeff, // bright sky color
        0x202020, // dim ground color
        5, // intensity
      );
    
      scene.add( ambientLight );

    
    //create a directional light
    const light = new THREE.DirectionalLight (0xffffff,5.0);

    //move the light
    light.position.set(10,10,10);
    
    //add the light to the scene
    scene.add(light); */
}



function createMaterials() {
    // le corps
    const body = new THREE.MeshStandardMaterial( {
        color: 0x000000, // black
        flatShading: true,
    } );

    // just as with textures, we need to put colors into linear color space
    body.color.convertSRGBToLinear();
    
    // la tete
    const head = new THREE.MeshStandardMaterial( {
        color: 0xf88e55, // darkgrey
        flatShading: true,
    } );
    
    head.color.convertSRGBToLinear();

    //la planete
    const planet = new THREE.MeshStandardMaterial( {
        color: 0x0000ff, //blue
        flatShading: true,
    });

    planet.color.convertSRGBToLinear();
    
    return {body,head,planet,};
}

function createGeometries () {
    const chest = new THREE.BoxBufferGeometry(1,2,0.5);
    const tete = new THREE.SphereBufferGeometry(0.4);

    const earth = new THREE.SphereBufferGeometry(5);

    const cube1 = new THREE.BoxBufferGeometry(1,1,1);

    return {chest,tete,earth,cube1,};
}

function createMeshes () {

    // create a Group to hold the pieces of the human
    const human = new THREE.Group();
    scene.add( human );

    // create another group to hold the planet + the items on it
    const rotating_planet = new THREE.Group();
    scene.add(rotating_planet);

    // the item we will control
    rotating_planet.name = 'controlled_item';


    const geometries = createGeometries();
    const materials = createMaterials();

    const chest = new THREE.Mesh( geometries.chest, materials.body );
    chest.position.set(7,0,0);

    const tete = new THREE.Mesh( geometries.tete, materials.head );
    tete.position.set(7,1.4,0);

    const earth = new THREE.Mesh(geometries.earth, materials.planet);
    earth.position.set(0,0,0);

    const cube = new THREE.Mesh(geometries.cube1, materials.planet);
    cube.position.set(0,5.5,0);
    human.position.set(0,0,0);

    human.add(
        chest,
        tete,
    );

    rotating_planet.add(
        earth,
        cube,
    )

/*
    // create a geometry
    //const geometry = new THREE.SphereBufferGeometry(2);
    // create a default  Standard material
    //const material = new THREE.MeshStandardMaterial( {color: 0xff0000});
    // create a Mesh containing the geometry and material
    //mesh = new THREE.Mesh( geometry, material );
    // add the mesh to the scene
    //scene.add(mesh);*/
}




// this allow to control the item `controlled_item` by pressing the keyboard arrows
document.addEventListener('keydown', (onkeydown));
function setupKeyControls() {
    var cube = scene.getObjectByName('controlled_item');
    document.onkeydown = function(e) {

        switch (e.keyCode) {

            case 37:
            cube.rotation.y -= 0.1;
            break;
            case 38:
            cube.rotation.x -= 0.1;
            break;
            case 39:
            cube.rotation.y += 0.1;
            break;
            case 40:
            cube.rotation.x += 0.1;
            break;
        }
    };
}












function createRenderer () {
    // create the renderer
    renderer = new THREE.WebGLRenderer({antialias: true});

    renderer.setSize( container.clientWidth, container.clientHeight );
    renderer.setPixelRatio( window.devicePixelRatio );

    renderer.physicallyCorrectLights = true;

    // add the automatically created <canvas> element to the page
    container.appendChild( renderer.domElement );
}


function createControls () {
    controls = new THREE.OrbitControls( camera, container );
}

//this will set up everything we need
function init() {
    // Get a reference to the container element that will hold our scene
    container = document.querySelector( '#scene-container' );

    // create a Scene
    scene = new THREE.Scene();

    scene.background = new THREE.Color('green');

    createCamera();
    createControls();
    createLights();
    createMeshes();
    createRenderer();

    //start the animation loop 
    renderer.setAnimationLoop( () => {
        update();
        render();
    }

    );
}

//this will update the scene when called
function update() {
    setupKeyControls();
}

//render, aka draw, an image of the scene
function render() {
    renderer.render(scene, camera);
}

//this will resize the windows when needed
function onWindowResize() {

    // set the aspect ratio to match the new browser window aspect ratio
    camera.aspect = container.clientWidth / container.clientHeight;

    // update the camera's frustum (use that every time you make any change to one of the parameters of the camera)
    camera.updateProjectionMatrix();

    // this updates the size of both canvas and renderer
    renderer.setSize( container.clientWidth, container.clientHeight );

}
  
window.addEventListener( 'resize', onWindowResize );


//setting up everything
init();
